'use-strict';

const path = require('path');

const NodemonPlugin = require('nodemon-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const nodeExternals = require('webpack-node-externals');

module.exports = (env = {}) => {
  const config = {
    entry: path.resolve(__dirname, './src/main.js'),
    output: { path: path.join(__dirname), filename: 'dist/main.js' },
    mode: env.development ? 'development' : 'production',
    target: 'node',
    devtool: env.development ? 'eval-source-map' : false,
    resolve: {
      modules: ['node_modules', 'src', 'package.json'],
      alias: {
        logger: path.join(__dirname, 'src', 'config/winston'),
        'mongo-models': path.join(__dirname, 'src', 'mongoose/models'),
        rabbit: path.join(__dirname, 'src', 'config/amqp'),
        errors: path.join(__dirname, 'src', 'utils/errors'),
      },
    },
    externals: [nodeExternals()],
    plugins: [],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ['@babel/plugin-transform-runtime'],
            },
          },
        },
        {
          test: /\.graphql?$/,
          loader: 'webpack-graphql-loader',
          options: {
            minify: env.development,
          },
        },
      ],
    },
    node: {
      __dirname: true,
    },
    watch: false,
  };
  if (env.development) {
    config.plugins.push(
      new Dotenv({
        path: './src/config/env/.env.development',
      }),
    );
  }

  if (env.nodemon) {
    config.watch = true;
    config.plugins.push(
      new NodemonPlugin({ watch: path.resolve('./src'), ext: 'js,graphql', ignore: ['*.test.js'] }),
    );
  }
  return config;
};
