# USERS-AMAP

This is a graphql api server to manage users. It saves them to mongoDB Cloud with mongoose and updates them after sending a rabbitmq message to the mailer with a JWT if it's a sensible data.

## GETTIND STARTED

Be sure npm, nodeJs (13.14.0), mongoDB (for unit tests) and Heroku are installed.

To install and use Heroku, please follow [THIS LINK](https://devcenter.heroku.com/articles/getting-started-with-nodejs).

```sh
git clone https://gitlab.com/b_fritz/users-amap.git
cd users-amap
npm i
```

Add a .env.development file in `src/config/env` and follow .env.example instructions.

Test your installation: `npm start:dev`

To test localy with Heroku: `heroku local web`

## USAGES

- _config:_
    This folder contains all configuration's files required to run the server (express, graphql, winston, amqp, db).
- _mongoose:_
    This folder contains mongoose's models.
- _schemas:_
    This folder contains graphql schemas for incoming requests.
- _services:_
    This folder contains the business logic for the graphql's routes.
- _utils:_
    Some global helpers like errors' messages.

## TODO LIST

### OK => changer la connection des routes

- `https://graphql.org/code/#javascript`

### OK => connecter à mongo

### OK => premier schéma mongoose

### OK => connecter à mongo sur `https://cloud.mongodb.com/v2/6038a025a8f106486e134dc7#clusters`

### OK => faire le CRUD

### OK => connecter en local à riddik

### OK => gérer les retours d'erreur

- mongoose: uniqueKey dupliquées
- mongoose: document not fund
- mongoose: update field not fund
- mongoose: perte de connection avec la BDD

### OK => premiers tests rabbitmq, connecter au cloud rabbitMq

### OK => connecter le tout sur heroku

### OK => faire la doc `https://nordicapis.com/graphql-documentation-generators-explorers-and-tools/`

### some cleans

- OK => nettoyer les devDependcies
- OK => renommer .spec en .test
- OK => remettre la promesse dans le main.js
- OK => utiliser utils/errors
- OK => bouger le .env + config heroku + .gitignore
- OK => remplacer ENVIRONMENT par NODE_ENV

### premier package npm: rabbitmq

- OK => faire fonctionner un envoie de message en dur dans un service
- faire un module, l'implémenter
- publier le module
