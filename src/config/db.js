import mongoose from 'mongoose';
import Promise from 'bluebird';
import _set from 'lodash/set';
import util from 'util';

mongoose.Promise = Promise;

/**
 * Initialize Mongoose.
 * @return { Promise }
 */
export default () =>
  new Promise((resolve, reject) =>
    mongoose.connect(
      process.env.DB_URI,
      { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
      err => {
        if (err) return reject(err);
        console.info('Mongoose connected');

        // print mongoose logs in dev env
        if (process.env.MONGOOSE_DEBUG) {
          _set(mongoose, 'debug', (collectionName, method, query, doc) =>
            console.debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc),
          );
        }
        resolve();
      },
    ),
  ).catch(({ message }) => console.error(message));
