/**
 * Configure graphql
 * @module config/graphql
 */

import { readFileSync } from 'fs';
const { buildSchema } = require('graphql');

import { getUser, addUser, getUsers, updateUser, deleteUser, confirmUpdate } from '../services';

const schema = buildSchema(
  readFileSync('src/schemas/schemas.graphql', { encoding: 'utf8', flag: 'r' }),
);

/**
 * Binds routes and actions
 */
const root = {
  getUser: ({ user_id }) => getUser(user_id),
  getUsers: () => getUsers(),
  addUser: ({ email }) => addUser(email),
  updateUser: data => updateUser(data),
  confirmUpdate: ({ token }) => confirmUpdate(token),
  deleteUser: ({ user_id }) => deleteUser(user_id),
};

export { schema, root };
