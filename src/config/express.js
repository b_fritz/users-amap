/**
 * Configure express application.
 * @module config/express
 */
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import expressWinston from 'express-winston';
import helmet from 'helmet';
import requestLanguage from 'express-request-language';
import { graphqlHTTP } from 'express-graphql';

import { schema, root } from './graphql.js';
import winstonInstance from './winston';

/**
 * Configure middlewares.
 * @param { Object } app The express application
 * @returns { Void }
 */
const _configureMiddlewares = app => {
  app.use(bodyParser.json());
  app.use(bodyParser.text({ type: 'application/graphql' }));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(
    helmet({ contentSecurityPolicy: process.env.NODE_ENV === 'production' ? undefined : false }),
  );
  app.use(cookieParser());
  app.use(requestLanguage({ languages: ['en'] }));
  app.use(cors());
};

/**
 * Configure specific dev environment. To show beautiful logs.
 * @param { Object } app The express application
 * @returns { Void }
 */
const _configureEnvironment = app => {
  // enable detailed API logging in dev env
  if (process.env.NODE_ENV === 'development') {
    expressWinston.requestWhitelist.push('body');
    expressWinston.responseWhitelist.push('body');
    app.use(
      expressWinston.logger({
        winstonInstance,
        msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
      }),
    );
  } else {
    // send to kibana in production
  }
};

export default () => {
  const app = express();
  _configureMiddlewares(app);
  _configureEnvironment(app);

  app.use(
    '/graphql',
    graphqlHTTP({
      schema,
      rootValue: root,
      graphiql: process.env.NODE_ENV === 'development',
    }),
  );
  return app;
};
