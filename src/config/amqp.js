import amqp from 'amqplib/callback_api';

const { AMQP_URI } = process.env;
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];

function closeOnErr(err) {
  if (!err) return false;
  console.error('[AMQP] error', err);
  amqpConn.close();
  return true;
}

function rabbitPublish(exchange, routingKey, content) {
  try {
    const published = pubChannel.publish(
      exchange,
      routingKey,
      Buffer.from(JSON.stringify(content)),
      { persistent: true },
      err => {
        if (err) {
          console.error('[AMQP] publish', err);
          offlinePubQueue.push([exchange, routingKey, content]);
          return pubChannel.connection.close();
        }
        // Add a consume to get mailer's errors/status
      },
    );
    return published;
  } catch (e) {
    console.error('[AMQP] publish', e.message);
    offlinePubQueue.push([exchange, routingKey, content]);
  }
}

function startPublisher() {
  amqpConn.createConfirmChannel((err, ch) => {
    if (closeOnErr(err)) return;
    ch.on('error', err => {
      console.error('[AMQP] channel error', err.message);
    });
    ch.on('close', () => {
      console.debug('[AMQP] channel closed');
    });

    pubChannel = ch;
    while (offlinePubQueue.length) {
      const [exchange, routingKey, content] = offlinePubQueue.shift();
      rabbitPublish(exchange, routingKey, content);
    }
  });
}

function startRabbit() {
  amqp.connect(AMQP_URI, (err, conn) => {
    if (err) {
      console.error('[AMQP]', err.message);
      return setTimeout(startRabbit, 1000);
    }
    conn.on('error', err => {
      if (err.message !== 'Connection closing') {
        console.error('[AMQP] conn error', err.message);
      }
    });
    conn.on('close', () => {
      console.error('[AMQP] reconnecting');
      return setTimeout(startRabbit, 1000);
    });
    console.info('[AMQP] connected');
    amqpConn = conn;
    startPublisher();
  });
}

export { startRabbit, rabbitPublish };
