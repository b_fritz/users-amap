const errors = {
  UNKNOW_USER: 'UNKNOW_USER',
  DUPLICATED_KEY: 'DUPLICATED_KEY',
};

export default errors;
