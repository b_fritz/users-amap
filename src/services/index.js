import _set from 'lodash/set';
import jwt from 'jsonwebtoken';

import { User } from 'mongo-models';
import { Types } from 'mongoose';
import { rabbitPublish } from 'rabbit';
import errors from '../utils/errors.js';

const { JWT_SECRET } = process.env;

/**
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {String} id Users unique ID.
 *
 * @apiDescription Request single user information
 *
 * @apiSuccess {Object} User
 *
 * @apiError UNKNOW_USER The <code>id</code> of the User was not found.
 */
export async function getUser(user_id) {
  const user = await User.findOne({ _id: Types.ObjectId(user_id) });
  if (user === null) return new Error(errors.UNKNOW_USER);
  return user;
}

/**
 * @apiName GetUsers
 * @apiGroup User

 * @apiDescription Request Users list.
 *
 * @apiSuccess {Array} User
 */
export async function getUsers() {
  const users = await User.find();
  return users;
}

/**
 * @apiName addUser
 * @apiGroup User
 *
 * @apiParam {String} email Users unique email.
 *
 * @apiDescription Add new user. If register works, it should send a confirmations
 * email with a jwt token wich one expires after 7 days.
 *
 * @apiSuccess {Object} User
 *
 * @apiError DUPLICATED_KEY the User already exists.
 */
export async function addUser(email) {
  const validationToken = jwt.sign({ email }, JWT_SECRET, { expiresIn: '7d' });
  const user = new User({
    first_name: null,
    last_name: null,
    password: {
      value: null,
      validationToken: null,
      isValid: false,
    },
    address: null,
    email: {
      value: email,
      validationToken,
      isValid: false,
    },
    phone: null,
  });
  await user.save();
  await rabbitPublish('', 'users', { validationToken, to: email, key: 'register' });
  return user;
}

/**
 * @apiName confirmUpdate
 * @apiGroup User
 *
 * @apiParam {String} token jwt token with sensible data to update.
 *
 * @apiDescription If register works, it should send a confirmations email with
 * a jwt token wich one expires after 7 days.
 *
 * @apiSuccess {Object} User
 *
 * @apiError UNKNOW_USER The <code>user</code> doesn't exists.
 * @apiError WRONG_TOKEN Wrong <code>token</code> .
 * @apiError EXPIRED_TOKEN Expired <code>token</code> .
 */
export async function confirmUpdate(token) {
  const decoded = jwt.verify(token, JWT_SECRET); // should contain email or password
  const user = await User.findOne({ email: decoded.email });
  if (user.email.validationToken === token) {
    // @todo: updates right value, remove validationToken attribute, turn isValid to true
  } else {
    // @todo: throw an error: wrong or expired token, not found (email)
  }
  console.debug({ user });
  return user;
}

/**
 * @apiName updateUser
 * @apiGroup User
 *
 * @apiParam {String} user_id jwt users id.
 * @apiParam {String} fields.key field to update.
 * @apiParam {String} fields.value new value.
 *
 * @apiDescription Updates users informations. If email or password are supplied,
 * send an email to confirm.
 *
 * @apiSuccess {Object} User
 *
 * @apiError UNKNOW_USER The <code>user</code> doesn't exists.
 */
export async function updateUser({ user_id, fields }) {
  // @todo: protect email and password updates
  const user = await getUser(user_id);
  fields.forEach(({ key, value }) => _set(user, key, value));
  try {
    await user.save();
  } catch (e) {
    // console.debug(e);
  }
  return user;
}

/**
 * @apiName deleteUser
 * @apiGroup User
 *
 * @apiParam {String} user_id jwt users id.
 *
 * @apiDescription Deletes an user.
 *
 * @apiSuccess {null} null
 *
 * @apiError UNKNOW_USER The <code>user</code> doesn't exists.
 */
export async function deleteUser(user_id) {
  // @todo: need to be protected?
  const { ok, deletedCount } = await User.deleteOne({ _id: Types.ObjectId(user_id) });
  if (!deletedCount) return new Error(errors.UNKNOW_USER);
  if (!ok) return new Error();
  return null;
}
