const mongoose = require('mongoose');

import { addUser, getUser, getUsers, updateUser, deleteUser } from './index.js';
import { User } from 'mongo-models';
import errors from '../utils/errors.js';

const DATABSE_NAME = 'test-users-amap';

jest.mock('jsonwebtoken', () => ({
  sign: () => 'mock-jwt-token',
}));

const mockRabbitPublish = jest.fn();
jest.mock('rabbit', () => ({
  rabbitPublish: (...rest) => mockRabbitPublish(rest),
}));

const FAKE_OBJECT_ID = '606184186208cd04cf9be2dc'; // wrong ObjectId

// @todo: test confirmUpdate
describe('users-services', () => {
  const { UNKNOW_USER } = errors;
  const defaultUser = {
    first_name: null,
    last_name: null,
    password: {
      value: null,
      isValid: false,
      validationToken: null,
    },
    email: {
      value: 'test-users-amap@test.fr',
      isValid: false,
      validationToken: 'mock-jwt-token',
    },
    address: null,
    phone: null,
  };

  beforeAll(async () => {
    const url = `mongodb://127.0.0.1/${DATABSE_NAME}`;
    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  });

  afterEach(async () => {
    await User.deleteMany();
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  describe('getUser', () => {
    it('should find an user', async () => {
      const addedUser = await addUser('test-users-amap@test.fr');
      const user = await getUser(addedUser._id);
      expect(user.email.value).toEqual(defaultUser.email.value);
    });

    it('should not find an user', async () => {
      await addUser('test-users-amap@test.fr');
      const user = await getUser(FAKE_OBJECT_ID);
      expect(user.message).toBe(UNKNOW_USER);
    });
  });

  describe('getUsers', () => {
    it('should find 2 users', async () => {
      await addUser('test-users-amap1@test.fr');
      await addUser('test-users-amap2@test.fr');
      const users = await getUsers();
      expect(users.length).toEqual(2);
    });
  });

  describe('addUser', () => {
    beforeEach(() => {
      mockRabbitPublish.mockClear();
    });

    it('should add an user', async () => {
      const user = await addUser('test-users-amap@test.fr');
      expect(user.email).toEqual(defaultUser.email);
      expect(mockRabbitPublish).toHaveBeenCalledWith([
        '',
        'users',
        { key: 'register', to: 'test-users-amap@test.fr', validationToken: 'mock-jwt-token' },
      ]);
    });

    it('should not add an existing user', async () => {
      await addUser('test-users-amap@test.fr');
      try {
        await addUser('test-users-amap@test.fr');
      } catch (e) {
        expect(mockRabbitPublish).toHaveBeenCalledTimes(1);
      }
    });
  });

  describe('updateUser', () => {
    it('should update an user', async () => {
      const user = await addUser('test-users-amap@test.fr');
      await addUser('test-users-amap2@test.fr');
      const fields = [
        { key: 'email.value', value: 'new-email@test.fr' },
        { key: 'phone', value: 'phone' },
      ];
      const updatedUser = await updateUser({ user_id: user.id, fields });
      expect(updatedUser.email.value).toBe(fields[0].value);
      expect(updatedUser.phone).toBe(fields[1].value);
    });

    it('should return an error', async () => {
      await addUser('test-users-amap@test.fr');
      const fields = [
        { key: 'email.value', value: 'new-email@test.fr' },
        { key: 'phone', value: 'phone' },
      ];
      const updatedUser = await updateUser({ user_id: FAKE_OBJECT_ID, fields });
      expect(updatedUser.message).toBe(UNKNOW_USER);
    });
  });

  describe('deleteUser', () => {
    it('should deleteUser an user', async () => {
      const user = await addUser('test-users-amap@test.fr');
      const deteledUser = await deleteUser(user.id);
      expect(deteledUser).toBe(null);
    });

    it('should throw an UNKNOW_USER error', async () => {
      await addUser('test-users-amap@test.fr');
      const deteledUser = await deleteUser(FAKE_OBJECT_ID);
      expect(deteledUser.message).toBe(UNKNOW_USER);
    });
  });
});
