import http from 'http';
import initApp from './config/express';
import logger from './config/winston';
import dbInit from './config/db';
import { startRabbit } from './config/amqp';

const launch = () =>
  new Promise((resolve, reject) =>
    dbInit()
      .then(async () => {
        const { PORT, NODE_ENV } = process.env;
        const app = initApp();
        const server = http.createServer(app);
        startRabbit();
        server.listen(PORT, () => logger.info(`Server started on port ${PORT} (${NODE_ENV})`));
        return server;
      })
      .catch(err => {
        logger.error(`An error occured: ${err}`);
        return reject(err);
      }),
  );

export default launch();
