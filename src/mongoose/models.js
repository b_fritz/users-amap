import mongoose, { Schema } from 'mongoose';
import errors from '../utils/errors.js';

const userSchema = new Schema(
  {
    first_name: String,
    last_name: String,
    password: {
      value: String,
      isValid: Boolean,
      validationToken: String,
    },
    email: {
      value: { type: String, unique: true },
      isValid: Boolean,
      validationToken: String,
    },
    address: String,
    phone: String,
  },
  { collection: 'users' },
);

userSchema.virtual('user_id').get(function () {
  return this._id; // @todo: Salt?
});

userSchema.post('exe', function (error, doc, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error(errors.DUPLICATED_KEY));
  } else {
    next();
  }
});

const User = mongoose.model('User', userSchema);

export { User };
